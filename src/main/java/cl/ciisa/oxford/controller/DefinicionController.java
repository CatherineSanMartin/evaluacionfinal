/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.ciisa.oxford.controller;

import cl.ciisa.oxford.model.domain.HistorialResponse;
import cl.ciisa.oxford.model.domain.ResultResponse;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author mono
 */
@WebServlet(name = "buscar", urlPatterns = {"/buscar"})
public class DefinicionController extends HttpServlet {

    private final Logger log = Logger.getLogger(this.getClass().getName());
    Gson gson = new Gson();
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet controller</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet controller at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        log.info("Inicio controller");

        String word = request.getParameter("txtSearch");
        String accion = request.getParameter("accion");
        log.info("Searching : " + word + " - accion : " + accion);

        if ("Buscar".equals(accion)) {
            procesaBuscar(request, response);
        } else if ("Historial".equals(accion)) {
            procesaHistorial(request, response);
        }

        log.info("Fin controller");
        return;
    }
    
    private void procesaBuscar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{
        List<String> definiciones = new ArrayList<>();
        Client client = ClientBuilder.newClient();
        String word = request.getParameter("txtSearch");
        String localUrl = request.getScheme() + "://"
                        + request.getServerName() + ":"
                        + request.getServerPort()
                        + request.getContextPath();
        log.info("Resource : " + localUrl);

        WebTarget myResource = client.target(localUrl + "/api/oxford/find/" + word);
        ResultResponse res = null;
        try {
            res = myResource.request(MediaType.APPLICATION_JSON).get(ResultResponse.class);
        } catch (NotFoundException e) {
            request.setAttribute("msgerror", "Palabra no existe");
            e.printStackTrace();
            request.getRequestDispatcher("index.jsp").forward(request, response);
            return;
        } catch (Exception e) {
            request.setAttribute("msgerror", "Error en proceso interno del servidor");
            e.printStackTrace();
            request.getRequestDispatcher("index.jsp").forward(request, response);
            return;
        }
        log.info("REST Response : " + gson.toJson(res));

        definiciones.addAll(res.getDefiniciones());

        log.info("Result : " + gson.toJson(definiciones));

        request.setAttribute("msgerror", null);
        request.setAttribute("definiciones", definiciones);
        request.setAttribute("palabra", word);
        request.getRequestDispatcher("index.jsp?accion=Buscar").forward(request, response);
        return;
    }
    
    private void procesaHistorial (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException{
        HistorialResponse res = new HistorialResponse();
        Client client = ClientBuilder.newClient();
        String localUrl = request.getScheme() + "://"
                        + request.getServerName() + ":"
                        + request.getServerPort()
                        + request.getContextPath();
        log.info("Resource : " + localUrl);

        WebTarget myResource = client.target(localUrl + "/api/oxford/history");

        try {
            res = myResource.request(MediaType.APPLICATION_JSON).get(HistorialResponse.class);
        } catch (Exception e) {
            request.setAttribute("msgerror", "Error en proceso interno del servidor");
            e.printStackTrace();
            return;
            //request.getRequestDispatcher("index.jsp?accion=Historial").forward(request, response);
        }
        log.info("REST Response : " + gson.toJson(res));

        request.setAttribute("historial", res.getHistorial());
        request.getRequestDispatcher("index.jsp?accion=Historial").forward(request, response);   
        return;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
