package cl.ciisa.oxford.model.domain;

public class MessageResponse {

    Long id;
    String object;
    String message;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MessageResponse(Long id, String object, String message) {
        this.id = id;
        this.object = object;
        this.message = message;
    }
    
}
